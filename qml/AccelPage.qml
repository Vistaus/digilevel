import QtQuick 2.9
import Ubuntu.Components 1.3

import "Components"

Page {
    header: PageTop { pageTitleId: 5 }
    Rectangle{
        width: parent.width
        color: 'gray'
        anchors {
            top: header.bottom
            bottom: undefined
            margins: units.gu(1)
        }
        Dial {
            id: _dialX
            anchors {
                margins: 0
                topMargin: units.gu(-4)
            }
            _borderCol: 'red'
            _dialText: "\n\nX"
            _rotation: 30
        }
        Dial {
            id: _dialY
            anchors {
                top: _dialX.verticalCenter
                topMargin: units.gu(2.5)
                left: _dialX.right
                leftMargin: units.gu(-6)
            }
            _borderCol: 'green'
            _dialText: "\n\nY"
            _rotation: -30
        }
        Dial {
            id: _dialZ
            anchors {
                top: _dialY.verticalCenter
                topMargin: units.gu(2.5)
                horizontalCenter: _dialX.horizontalCenter
                verticalCenterOffset: units.gu(4)
            }
            _borderCol: 'blue'
            _dialText: "\n\nZ"
            _rotation: -45
        }
        Clock {
            id: _clock
            anchors {
                top: _dialZ.verticalCenter
                topMargin: units.gu(2.5)
                horizontalCenter: _dialY.horizontalCenter
                verticalCenterOffset: units.gu(4)
            }
        }
    }
    Timer {
        running: parent.enabled
        interval: 100
        triggeredOnStart: true
        repeat: true
        onTriggered: {
            var currentTime = new Date()
            var cyclecount
            _dialX._rotation = ax * 18
            _dialY._rotation = ay * 18
            _dialZ._rotation = az * 18
            if ( ++cyclecount > 10 ) { cyclecount = 0 }
            _clock._seconds = currentTime.getSeconds() * 6
            _clock._minutes = currentTime.getMinutes() * 6 + currentTime.getSeconds() * 0.1
            _clock._hours = currentTime.getHours() * 30 + currentTime.getMinutes() * 0.5
        }
    }
}
