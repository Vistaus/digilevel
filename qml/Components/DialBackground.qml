import QtQuick 2.9

Rectangle {

    property alias _borderCol: _border.color
    anchors.centerIn: parent
    width: Math.min(parent.width, parent.height) - 20
    height: width
    radius: width / 2
    //color: "#eaeaea"
    color: 'transparent'
    border {
        id: _border
        width: width * 0.03
        color: "black"
     }
}
